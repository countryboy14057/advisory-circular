#!/bin/bash
set -e

dir=$(dirname "$0")
instance="$1"
config_dir=/etc/advisorycircular
run_dir=/var/lib/advisorycircular
ac_db_path=/usr/local/share/advisorycircular/aircraft-info/aircraft-info.sqb
interval=15

config_path="$config_dir"/"$instance"-config.yaml
secrets_path="$config_dir"/"$instance"-secrets.yaml
history_path="$run_dir"/"$instance"-history.json
(cd "$run_dir" && "$dir"/intervalexec $interval node $dir/out/advisorycircular.js --config "$config_path" --secrets "$secrets_path" --aircraft-info-db "$ac_db_path" --history "$history_path" --log-prefix "$instance")
